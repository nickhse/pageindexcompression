package com.ml;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.lang.System.out;

public class Main {

    public static void main(String[] args) throws IOException {

        TextParser textParser = new TextParser();
        Map<String, List<Integer>> indexPage = textParser.parseText("tom_1.txt");

        int sizeOrdinal = 0;
        int sizeUnary = 0;
        int sizeGamma = 0;
        int sizeDelta = 0;
        for (Map.Entry<String, List<Integer>> indexEntry : indexPage.entrySet()) {
            List<Integer> docIds = indexEntry.getValue();

            List<String> compressedDocIdsUnary = unaryAlgorithm(docIds);
            List<String> compressedDocIdsGamma = gammaAlgorithm(docIds);
            List<String> compressedDocIdsDelta = deltaAlgorithm(docIds);


            sizeOrdinal += docIds.size();
            sizeUnary += compressIntegers(compressedDocIdsUnary).size();
            sizeGamma += compressIntegers(compressedDocIdsGamma).size();
            sizeDelta += compressIntegers(compressedDocIdsDelta).size();
        }

        out.println("Ordinal index size : " + sizeOrdinal);
        out.println("Unary  index size : " + sizeUnary);
        out.println("Gamma index size : " + sizeGamma);
        out.println("Delta index size : " + sizeDelta);

    }

    private static List<String> unaryAlgorithm(List<Integer> docIds) {
        List<String> compressedDocIds = new ArrayList<String>();
        for (Integer docId : docIds) {
            String unaryString = "";
            for (int i = 0; i < docId; i++) {
                unaryString = unaryString + "1";
            }
            unaryString = unaryString.concat("0");
            compressedDocIds.add(unaryString);
        }
        return compressedDocIds;
    }

    private static List<String> gammaAlgorithm(List<Integer> docIds) {
        List<String> compressedDocIds = new ArrayList<String>();
        for (Integer docId : docIds) {

            String intToBinary = Integer.toBinaryString(docId);
            String result = StringUtils.repeat("0", intToBinary.length() - 1) + intToBinary;
            compressedDocIds.add(result);
        }
        return compressedDocIds;
    }

    private static List<String> deltaAlgorithm(List<Integer> docIds) {
        List<String> compressedDocIds = new ArrayList<String>();
        for (Integer docId : docIds) {

            String intToBinaryN = Integer.toBinaryString(docId);
            String intToBinaryL = Integer.toBinaryString(intToBinaryN.length());
            String intM = StringUtils.repeat("0", (intToBinaryL.length() - 1)) + 1;
            String result = intM + removeLeadOne(intToBinaryL) + removeLeadOne(intToBinaryN);
            compressedDocIds.add(result);
        }
        return compressedDocIds;
    }

    public static List<Integer> compressIntegers(List<String> compressedDocIds) {
        List<Integer> compressedInteger = new ArrayList<Integer>();
        StringBuilder item = new StringBuilder();

        for (String compressedDocId : compressedDocIds) {
            if ((item.length() + compressedDocId.length()) < 32) {
                item.append(compressedDocId);
            } else {
                if (StringUtils.isNotEmpty(item.toString())) {
                    int compressedInt = Integer.parseInt(item.toString(), 2);
                    compressedInteger.add(compressedInt);
                }
                item.setLength(0);
            }

        }

        if (item.length() != 0) {

            int compressedInt = Integer.parseInt(item.toString(), 2);
            compressedInteger.add(compressedInt);
            item.setLength(0);
        }
        return compressedInteger;
    }

    public static String removeLeadOne(String s) {
        if (s.charAt(0) == '1') {
            return s.substring(1);
        }
        return s;
    }
}
